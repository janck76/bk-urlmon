#!/usr/bin/perl
#===============================================================================
#
#         FILE:  urlget.pl
#
#        USAGE:  ./urlget.pl
#
#  DESCRIPTION:  Sjekker om Bergen Kommune nettsider er tilgjengelig
#                wget resultat skrives til wget_*.log for hver URL som sjekkes
#
#                Dersom det er problemer med internet linjen opprettes
#                det en fil med navn: internet_err.log
#                Denne slettes automatisk når problemet er løst
# 
#                Scriptet kjører på en maskin som står på ADSL linje:
#                adls01e (Webconsole: http://10.14.24.110/)
#
#                Scriptet kjører i crontab under bruker ikt
#
#                Dersom en fjerner/endrer URLer som sjekkes, husk å slett /var/log/url/*.log
#                og /trans/urlmon/*.log
#                Ellers vil det varsles på gamle URLer
#
#===============================================================================

use strict;
use warnings;
use Digest::MD5 qw(md5_hex);
use File::Copy;


my $DESTDIR     = '/var/log/urlmon';
my $TIMEOUT     = 10;
my @BK_URL      = qw(https://svarut.ks.no/ https://mas.bergen.kommune.no/zdm/login_xdm_uc.jsp https://mam.bergen.kommune.no/vpn/index.html https://www.bergen.kommune.no/ http://www.bergenskart.no/bergen/);
my @EXT_URL     = qw(https://www.google.com/ https://altinn.no/ https://idporten.difi.no/);
#my $LOG         = '/var/log/urlmon/urlmon.log';
my $GID         = 1000;

my $EXT_DRIVE   = '/trans/urlmon';
my $UPDATING    = "$EXT_DRIVE/updating";

umask 0007;

sub update_start() {
    open my $fh, ">$UPDATING";
    close $fh;
    system 'sync';
    return;
}

sub update_finished {
    unlink $UPDATING;    
    system 'sync';
    return;
}

sub copy_files {
    my $r=1;

    foreach my $file (glob "$DESTDIR/*.log") {
        print "copy $file to $EXT_DRIVE\n";
        unless ( copy($file, $EXT_DRIVE) ) {
            warn $!;
            $r=0;
        }
    }
    my @files = glob "$EXT_DRIVE/*.log";
    chown -1, $GID, @files;
    return $r;
}


#===============================================================================

# Simulere brudd mot internett
#$ENV{http_proxy} = 'localhost:80';

# Sjekk om vi når ting på Internet
my $internet_reachable = 0;
foreach (@EXT_URL) {
    my $url  = $_;
    my $file = "$DESTDIR/wget_" . md5_hex($url);
    my $log  = "$file.log";
    #my $tmp  = "$file.tmp";
    my $html = "$file.html";

    unlink $html if ( -f $html );

    #if ( -f $log ) {
    #    move($tmp, $log);
    #       `tail -100 $tmp >$log`;
    #    chown -1, $GID, $log;
    #}

    my $t1=time;
    `wget --no-check-certificate --max-redirect=200 --timeout=$TIMEOUT --tries=1 -a "$log" -O $html $url`;
    chown -1, $GID, ($log,$html);
    my $exit=$?>>8;
    my $t2=time;
    my $dt=$t2-$t1;

    open my $fh, ">>$log" or die "Can't open $log $!";
    print $fh "EXIT:$exit\n";
    print $fh "TIME:$dt\n\n";
    close $fh;

    if( $exit > 0) {
        `cat /tmp/itest.log >>$DESTDIR/internet_err.log`;
        chown -1, $GID, "$DESTDIR/internet_err.log";
        copy("$DESTDIR/internet_err.log", $EXT_DRIVE);
    }
    else {
        $internet_reachable = 1;
    }

    #move($tmp, $log);
}
system 'sync';

exit unless $internet_reachable;

unlink "$DESTDIR/internet_err.log" if( -f "$DESTDIR/internet_err.log");
unlink "$EXT_DRIVE/internet_err.log" if( -f "$EXT_DRIVE/internet_err.log");

# Sjekk om vi når ting ved Bergen Kommune
foreach (@BK_URL) {
    my $url  = $_;
    my $file = "$DESTDIR/wget_" . md5_hex($url);
    my $log  = "$file.log";
    #my $tmp  = "$file.tmp";
    my $html = "$file.html";

    unlink $html if ( -f $html );

    #if ( -f $log ) {
    #    move($tmp, $log);
    #       `tail -100 $tmp >$log`;
    #    chown -1, $GID, $log;
    #}

    my $t1=time;
    `wget --no-check-certificate --max-redirect=0 --timeout=$TIMEOUT --tries=1 -a "$log" -O $html $url`;
    chown -1, $GID, ($log,$html);
    my $exit=$?>>8;
    my $t2=time;
    my $dt=$t2-$t1;

    open my $fh, ">>$log" or die "Can't open $log $!";
    print $fh "EXIT:$exit\n";
    print $fh "TIME:$dt\n\n";
    close $fh;

    #move($tmp, $log);
}


update_start();
copy_files() or die "Error copying file to $EXT_DRIVE\n";
update_finished();


